# docker-android-apps-cicd

docker-android-apps-cicd is an open-source fork of [gitlab-ci-android](https://github.com/jangrewe/gitlab-ci-android) adapted as per the requirements of the */e/* internal development team to develop and maintain projects which are built using GitLab's CI/CD. This project aims to offers a complete environment containing required tools to build Android applications.

## Development

- A the list of contributors can be viewed on this repository's [contributors graph](https://gitlab.e.foundation/e/apps/docker-android-apps-cicd/-/graphs/master).

In case you wish to contribute to the development of this project, feel free to open a [Merge Request](https://gitlab.e.foundation/e/apps/docker-android-apps-cicd/-/merge_requests) or an [Issue](https://gitlab.e.foundation/e/backlog/-/issues/) for the same. Contributions are always welcome.