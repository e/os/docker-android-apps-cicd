FROM ubuntu:20.04

# Global Gradle arguments
ARG GRADLE_VERSION="8.0"
ARG GRADLE_DIST=bin
ARG GRADLE_HOME="/gradle"
ARG GRADLE_PATH="${GRADLE_HOME}/gradle-${GRADLE_VERSION}/bin"

# Global Android SDK arguments and environment variables
ARG ANDROID_SDK_VERSION=7583922
ENV ANDROID_SDK_ROOT "/sdk"
ENV ANDROID_HOME "/sdk"
ARG ANDROID_CMDLINE_TOOLS_BIN="${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin"

# Add Gradle and SDK tools to the PATH
ENV PATH "$PATH:${ANDROID_CMDLINE_TOOLS_BIN}/:${GRADLE_PATH}/:${ANDROID_HOME}/emulator/:${ANDROID_HOME}/platform-tools/:${ANDROID_HOME}/build-tools/31.0.0/"

# Setup distribution and install required distribution packages
ENV DEBIAN_FRONTEND noninteractive

RUN dpkg --print-architecture && \
	dpkg --print-foreign-architectures

RUN dpkg --add-architecture i386 && \
	dpkg --print-foreign-architectures

RUN apt-get -qq update && \
    apt-get install -qqy --no-install-recommends \
      git-lfs \
      openssh-client \
      curl \
      git-core \
      openjdk-11-jdk \
      openjdk-17-jdk \
      libc6-i386 \
      libstdc++6:i386 \
      zlib1g:i386 \
      unzip \
      make \
      locales \
      autoconf \
      automake \
      libtool \
      pkg-config \
      wget \
      gcc \
      libsonic-dev \
      libpcaudio-dev \
      zip \
      jq \
      aapt \
      apksigner 

RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.UTF-8

RUN rm -f /etc/ssl/certs/java/cacerts; \
    /var/lib/dpkg/info/ca-certificates-java.postinst configure

# Download and install SDK command-line tools
RUN mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools && \
    wget -q https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_VERSION}_latest.zip && \
    unzip *tools*linux*.zip -d ${ANDROID_SDK_ROOT}/cmdline-tools && \
    mv ${ANDROID_SDK_ROOT}/cmdline-tools/cmdline-tools ${ANDROID_SDK_ROOT}/cmdline-tools/tools && \
    rm *tools*linux*.zip

RUN yes | ${ANDROID_CMDLINE_TOOLS_BIN}/sdkmanager --licenses

RUN mkdir -p /root/.android \
 && touch /root/.android/repositories.cfg \
 && sdkmanager --verbose --update

ADD packages.txt /sdk
RUN sdkmanager --verbose --package_file=/sdk/packages.txt

# Download and install Gradle
# Keep it aligned default version as given in https://developer.android.com/studio/releases/gradle-plugin#versioning-update
RUN wget -q https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-${GRADLE_DIST}.zip -O /gradle.zip && unzip /gradle.zip -d /gradle
